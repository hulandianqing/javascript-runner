/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-this-alias */

/*
 * 防抖函数
 * @param fn 事件触发后的回调函数
 * @param delay 延迟时间
 */
export function debounce(fn: (...args: any[]) => any, delay = 200): (...args: any[]) => void {
  let timeout: number;
  return function (...args: any[]) {
    clearTimeout(timeout);
    const ctx = this;
    timeout = setTimeout(function () {
      fn.apply(ctx, args);
    }, delay);
  };
}

/**
 * 系统初始化等待函数
 */
export function onSystemReady() {
  return new Promise((resolve: any) => {
    const { utools } = window as any;
    if (utools) {
      utools.onPluginReady(() => resolve());
    } else {
      resolve();
    }
  });
}
