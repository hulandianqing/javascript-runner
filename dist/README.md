# JavaScript Runner

#### 介绍

> 该项目可作为网页端独立运行，也可作为 `uTools` 插件运行，更加方便启动和快捷调试代码。
>
> 项目使用 `Vue3` 开发，编辑器采用 `monaco-editor`。

JavaScript Runner 左侧为代码编辑器，右侧为日志 `console.log` 输出区域，点击右下角运行按钮，测试代码输入输出逻辑，快捷验证代码思路。

![demo](./resources/demo.png)

#### 特点 & 版本

**V0.0.5**

[BugFix] 修复代码末端存在注释时运行报错问题

**V0.0.4**

[BugFix] 优化代码本地持久化功能

**V0.0.3**

1. 支持编辑器与日志区宽度调整
2. 支持显示、隐藏日志区（快捷键 `F12` ）

**V0.0.2**

1. 支持代码本地持久化
2. 支持代码选中部分单独执行

**V0.0.1**

1. 支持 ES6 / ES7 语法
2. 支持代码高亮，代码提示，格式化代码
3. 支持快捷键操作，支持全文搜索、替换

#### 常用快捷键

| 功能       | 快捷键                |
| ---------- | --------------------- |
| 运行代码   | `Ctrl` + `R`          |
| 代码格式化 | `Shift` + `Alt` + `F` |
| 查找       | `Ctrl` + `F`          |
| 替换       | `Ctrl` + `H`          |

#### 完整代码

[焚霜/javascript-runner (gitee.com)](https://gitee.com/chronosmaker/javascript-runner)

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
